var ColorPicker = {
	value: 0xffffff,
	
	h: 1,
	s: 0,
	v: 1,
	
	init: function (element) {
		var me = ColorPicker;
		
		me.main = element;

		me.offset = me.getPosition(element, document.body);
		
		var panel = $('div.cp-panel', me.main);
		panel.on('mousedown', me._panelDragHandler);
		
		var bar = $('div.cp-hue-bar', me.main);
		bar.on('mousedown', me._barDragHandler);
		
		$('div.cp-input-rgb input', me.main)
			.on('input', me._RGBInputKeyFixHandler)
			.on('keydown', me._RGBInputKeyShortHandler);
		
		$('div.cp-result input', me.main)
			.on('change', me._ColorInputKeyFixHandler);
		
		$('div.cp-confirm button', me.main).on('click', function (ev) {
			if (me.onconfirmcolor(me.value) !== false) {
				me.hide();
			}
		});

		me.pos = {
			panel: $.extend(me.getPosition(panel[0], me.main), {
				offsetX: parseInt(panel.css('border-left-width'), 10),
				offsetY: parseInt(panel.css('border-top-width'), 10),
				width: 255,
				height: 255
			}),
			bar: $.extend(me.getPosition(bar[0], me.main), {
				offsetX: parseInt(bar.css('border-left-width'), 10),
				offsetY: parseInt(bar.css('border-top-width'), 10),
				width: 20,
				height: 255
			})
		};
		
		me.setColor(me);
	},

	getPosition: function (el, refer) {
		var pos = {x: 0, y: 0};
		
		var cStyle = el.currentStyle || document.defaultView.getComputedStyle(el, null);
		
		// var layoutBWX = 0, layoutBWY = 0;
		var isWebkit = navigator.userAgent.match(/Chrome|Safari/);
		
		if (!refer) {
			if (cStyle.position == 'absolute') {
				pos.x = el.offsetLeft - (parseInt(cStyle.marginLeft, 10) || 0);
				pos.y = el.offsetTop - (parseInt(cStyle.marginTop, 10) || 0);
			} else if (cStyle.position == 'relative') {
				pos.x = (parseInt(cStyle.left, 10) || 0);
				pos.y = (parseInt(cStyle.top, 10) || 0);
			}
		} else {
			for (var node = el; node.offsetParent && node != refer; node = node.offsetParent) {
				pos.x += node.offsetLeft;
				pos.y += node.offsetTop;

				// bug when border-box under Chrome 34 (also in webkit)
				if (isWebkit) {
					var pStyle = document.defaultView.getComputedStyle(node.offsetParent, null);
					if (pStyle['box-sizing'] == 'border-box') {
						pos.x += (parseInt(pStyle.borderLeftWidth, 10) || 0);
						pos.y += (parseInt(pStyle.borderTopWidth, 10) || 0);
					}
				}
			}
			//避免ie和ff计算body的offsetLeft不一致
	//		pos.x = el.offsetLeft - node.offsetLeft;//-(parseInt(cStyle.marginLeft)||0);
	//		pos.y = el.offsetTop - node.offsetTop;//-(parseInt(cStyle.marginTop)||0);
			if (cStyle.position == 'static' && el.currentStyle) {
				pos.x += (parseInt(document.body.currentStyle.marginLeft, 10) || 0) * 2;
				pos.y += (parseInt(document.body.currentStyle.marginTop, 10) || 0) * 2;
			}
		}
		pos.left = pos.x;
		pos.top = pos.y;
		
		return pos;
	},
	
	calculateCoordinate: function (id, x, y) {
		var me = ColorPicker;

		var pos = me.pos[id];

		var offsetX = pos.offsetX || 0;
		var offsetY = pos.offsetY || 0;
		var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
		var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);

		var innerX = x + scrollLeft - (me.offset.x + pos.x + pos.offsetX);
		var innerY = y + scrollTop - (me.offset.y + pos.y + pos.offsetY);
		
		return {
			x: me.range(innerX, 0, pos.width),
			y: me.range(innerY, 0, pos.height)
		};
	},
	
	updatePanelCoordinate: function (x, y) {
		var me = ColorPicker;
		
		var pos = me.calculateCoordinate('panel', x, y);
		
		me.s = pos.x / 255;
		me.v = 1 - pos.y / 255;
		
		me.updateColor();
	},
	
	updateBarCoordinate: function (x, y) {
		var me = ColorPicker;
		
		var pos = me.calculateCoordinate('bar', x, y);
		
		me.h = 1 - pos.y / 255;
		
		me.updateColor();
	},
	
	updateColor: function () {
		var me = ColorPicker;
		
		var color = me.HSVToRGB(me.h, me.s, me.v);
		
		var rgb = me.toHexRGB(color);
		
		$('.cp-current', me.main).css('background-color', '#' + rgb);
		
		$('#cp-input-result').val(rgb);
		
		var pcX = me.s * 255;
		var pcY = (1 - me.v) * 255;
		$('.cp-panel-cursor', me.main).css({
			left: pcX + 'px',
			top: pcY + 'px'
		});
		
		var bcY = (1 - me.h) * 255;
		$('.cp-hue-cursor', me.main).css('top', bcY + 'px');
		
		var hueRGB = me.toHexRGB(me.HSVToRGB(me.h, 1, 1));
		$('.cp-panel', me.main).css('background-color', '#' + hueRGB);
		
		$('#cp-input-r').val(color.r || 255);
		$('#cp-input-g').val(color.g || 255);
		$('#cp-input-b').val(color.b || 255);
		
		me.value = rgb;
	},
	
	getHSVColor: function () {
		var me = ColorPicker;
		
		var color = $('.cp-input-rgb input', me.main).toArray().map(function (item) {
			return item.value;
		});
		
		return me.RGBToHSV.apply(me, color);
	},
	
	setColor: function (color) {
		var me = ColorPicker;
		
		$.extend(me, color, ['h', 's', 'v']);
		me.updateColor();
	},
	
	_panelDragHandler: function (ev) {
		var me = ColorPicker;
		
		function mover (ev) {
			me.updatePanelCoordinate(ev.clientX, ev.clientY);
			ev.preventDefault();
		}
		
		$(document).on('mousemove', mover);
		
		$(document).one('mouseup', function (ev) {
			$(this).off('mousemove', mover);
		});
		
		me.updatePanelCoordinate(ev.clientX, ev.clientY);
		
		ev.preventDefault();
	},
	
	_barDragHandler: function (ev) {
		var me = ColorPicker;
		
		function mover (ev) {
			me.updateBarCoordinate(ev.clientX, ev.clientY);
			ev.preventDefault();
		}
		
		$(document).on('mousemove', mover);
		
		$(document).one('mouseup', function (ev) {
			$(this).off('mousemove', mover);
		});
		
		me.updateBarCoordinate(ev.clientX, ev.clientY);
		
		ev.preventDefault();
	},
	
	_RGBInputKeyFixHandler: function (ev) {
		var me = ColorPicker;
		var value = me.value;
		
		me.value = me.range(parseInt(value.replace(/\D/g, ''), 10) || 0, 0, 255);
		
		if (me.value != value) {
			me.setColor(me.getHSVColor());
		}
	},
	
	_RGBInputKeyShortHandler: function (ev) {
		var me = ColorPicker;
		var value = me.value;
		if (ev.keyCode == 38 || ev.keyCode == 40) {
			me.value = me.range(parseInt(value, 10) + 39 - ev.keyCode, 0, 255);
			if (me.value != value) {
				me.setColor(me.getHSVColor());
			}
			
			ev.preventDefault();
		}
	},
	
	_ColorInputKeyFixHandler: function (ev) {
		var me = ColorPicker;
		
		me.value = me.value.replace(/[^0-9a-f]/ig, '');
		
		me.setHexRGBColor(me.value);
	},
	
	range: function (num, min, max) {
		return Math.max(min, Math.min(num, max));
	},
	
	toHex: function (value) {
		var ret = ('0' + value.toString(16));
		ret = ret.substr(ret.length - 2);
		return ret;
	},
	
	toHexRGB: function (color) {
		var me = ColorPicker;
		return [me.toHex(color.r), me.toHex(color.g), me.toHex(color.b)].join('');
	},
	
	// h:色相，s:饱和度，v:亮度 http://www.easyrgb.com/
	HSVToRGB: function(h, s, v){
		var r, g, b;
		if (s == 0) {
			r = v * 255;
			g = v * 255;
			b = v * 255;
		} else {
			var tempH = h * 6;
			if (tempH == 6) {
				tempH = 0;
			}
			tempI = Math.floor(tempH);
			temp_1 = v * (1 - s);
			temp_2 = v * (1 - s * (tempH - tempI));
			temp_3 = v * (1 - s * (1 - (tempH - tempI)));
			switch (tempI) {
			case 0:
				r = v;
				g = temp_3;
				b = temp_1;
				break;
			case 1:
				r = temp_2;
				g = v;
				b = temp_1;
				break;
			case 2:
				r = temp_1;
				g = v;
				b = temp_3;
				break;
			case 3:
				r = temp_1;
				g = temp_2;
				b = v;
				break;
			case 4:
				r = temp_3;
				g = temp_1;
				b = v;
				break;
			default:
				r = v;
				g = temp_1;
				b = temp_2;
				break;
			}
			r = r * 255;
			b = b * 255;
			g = g * 255;
		}
		return {
			r: Math.round(r),
			g: Math.round(g),
			b: Math.round(b)
		};
	},
	
	RGBToHSV: function (r, g, b) {
		var max = Math.max.apply(Math, arguments);
		var min = Math.min.apply(Math, arguments);
		var delta = max - min;
		var h, s, v;
		
		if (delta) {
			if (r == max) {
				h = (g - b) / delta;
			}
			if (g == max) {
				h = 2 + (b - r) / delta;
			}
			if (b == max) {
				h = 4 + (r - g) / delta;
			}
		} else {
			h = 0;
		}
		
		h *= 60;
		if (h < 0) {
			h += 360;
		}
		h /= 360;
		s = delta / max;
		v = max / 255;
		
		return {h: h, s: s, v: v};
	},

	setHexRGBColor: function (str) {
		var me = ColorPicker;

		var r = parseInt(str.slice(0, 2), 16) || 0,
			g = parseInt(str.slice(2, 4), 16) || 0,
			b = parseInt(str.slice(4, 6), 16) || 0;
		
		me.setColor(me.RGBToHSV(r, g, b));
	},

	setRGBColor: function (r, g, b) {
		var me = ColorPicker;
		me.setColor(me.RGBToHSV(r, g, b));
	},

	showAt: function (button) {
		var me = ColorPicker;
		var main = me.main;
		var pos = me.getPosition(button, document.body);

		pos.y += button.offsetHeight;

		$(main).css({
			top: pos.y + 'px',
			left: pos.x + 'px'
		}).addClass('active');

		me.offset = pos;

		var handler = {
			'#': me.setHexRGBColor,
			'rgb': me.setRGBColor
		};

		var color = button.style.backgroundColor || '#ffffff';
		var matcher = color.match(/^(#|rgb)(.*)/i);
		handler[matcher[1]].apply(me, matcher[2].replace(/[\(\)\s]/g, '').split(','));
	},

	hide: function () {
		$(ColorPicker.main).removeClass('active');
	}
};